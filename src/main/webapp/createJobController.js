(function(){
    
   var app = angular.module("mainApp",[]);
  
   var createJobController = function($scope, $http, resourcesService){
     
     var onFetchError = function(message){
       $scope.error = "Error Fetching Resources. Message:" +message;
     };
     
     var onFetchCompleted = function(data){
        $scope.resources =data;
     };
     
     var getUsers = function(){
       resourcesService.get().then(onFetchCompleted,onFetchError);
     };
     
     getUsers();     
     $scope.createJob = function(){
  	   $http({
      		method: "post",
      		url:"jobs",
      		data:{
      				"resourceType": $scope.resourceType,
      				"totalTask": $scope.jobCount,
      				"desc": $scope.description
      		}
	 		}).then(function success(data){
      		 console.log(data.data);
      		 $scope.jobdetails=data.data;
      		$location.path("/confirm");
      	 },function error(data){
      		 
      	 });
     }
	 $scope.submitJobFunc = function () {
	 $scope.myTxt = $scope.jobCount + " Job schedulled successfully for resource " + $scope.resourceType;
	 
	/* $http({
		method  : 'POST',
		url     : 'http://localhost:1234/rest/student/add?id=' + $scope.jobCount +'&name=' + $scope.resourceType		  		  
	 }).success(function(data) {
		console.log(data);			
	 });*/
	 
			 
	 
	 };
     
   };
 
   app.controller("createJobController", createJobController);
  
}());