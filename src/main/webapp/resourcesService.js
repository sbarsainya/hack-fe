(function(){
    
    var resourcesService = function($http) {      
      var getResources = function(username){
            return $http.get("/resources")
                        .then(function(response){
                           return response.data; 
                        });
      };
	  
      return {
          get: getResources
      };
	  
    };
    
    var module = angular.module("mainApp");
    module.factory("resourcesService", resourcesService);
    
}());