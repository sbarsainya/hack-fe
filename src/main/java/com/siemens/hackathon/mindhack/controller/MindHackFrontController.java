package com.siemens.hackathon.mindhack.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.siemens.hackathon.mindhack.model.JobDetails;
import com.siemens.hackathon.mindhack.model.Jobs;
import com.siemens.hackathon.mindhack.model.ResourceType;

@RestController
public class MindHackFrontController {

	@RequestMapping(value="/jobs", method=RequestMethod.GET)
	public Jobs[] getTheData(){
		System.err.println("setting data>>>>  " );
		RestTemplate restTemplate = new RestTemplate();
		Jobs[] jobs = (Jobs[]) restTemplate.getForObject("http://localhost:8080/jobs", Jobs[].class);
		System.err.println(jobs);
		return jobs;
	}
	@RequestMapping(value="/job/{id}", method=RequestMethod.GET)
	public JobDetails getJobsDetails(@PathVariable String id){
		System.err.println("setting data>>>>  " );
		RestTemplate restTemplate = new RestTemplate();
		JobDetails job =  restTemplate.getForObject("http://localhost:8080/job/"+id, JobDetails.class);
		System.err.println(job);
		return job;
	}
	@RequestMapping(value="/resources", method=RequestMethod.GET)
	public ResourceType[] getResources(){
		System.err.println("setting data>>>>  " );
		RestTemplate restTemplate = new RestTemplate();
		ResourceType[] resources =  restTemplate.getForObject("http://localhost:8280/resoucemgmnt-siemens/resourcetypes", ResourceType[].class);
		System.err.println(resources);
		return resources;
	}
	@RequestMapping(value="/submitJobs", method=RequestMethod.POST)
	public Object submitJob(@RequestBody JobDetails jobDetails){
		System.err.println("setting data>>>>  " + jobDetails);
		RestTemplate restTemplate = new RestTemplate();
		Object resources =  restTemplate.postForObject("http://localhost:8280/resoucemgmnt-siemens/jobs", jobDetails,Object.class);
		System.err.println(resources);
		return resources;
	}
}
