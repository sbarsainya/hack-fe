package com.siemens.hackathon.mindhack.model;

import java.io.Serializable;

public class Jobs implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6198172535790609973L;
	private String id;
	private String scheduleDate;
	private String percentage;
	public String getScheduleDate() {
		return scheduleDate;
	}
	public void setScheduleDate(String scheduleDate) {
		this.scheduleDate = scheduleDate;
	}
	public String getPercentage() {
		return percentage;
	}
	public void setPercentage(String percentage) {
		this.percentage = percentage;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
}
