package com.siemens.hackathon.mindhack.model;

import java.io.Serializable;
import java.time.LocalDate;


/**
 *
 * @author Prafful Dhoke
 */
public class JobDetails implements Serializable {

	@Override
	public String toString() {
		return "JobDetails [ name=" + name + ", desc=" + desc + ", totalTask=" + totalTask
				+ ", taskRemaining=" + taskRemaining + ", resourceType=" + resourceType + ", isCompleted=" + isCompleted
				+ ", endDate=" + endDate + ", startDate=" + startDate + "]";
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 2126089262485373173L;


	private String name;

	private String desc;

	private Long totalTask;

	private Long taskRemaining;

	private Long resourceType;

	private Boolean isCompleted;



	private LocalDate endDate;

	private LocalDate startDate;

	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public Long getTotalTask() {
		return totalTask;
	}

	public void setTotalTask(Long totalTask) {
		this.totalTask = totalTask;
	}

	public Long getTaskRemaining() {
		return taskRemaining;
	}

	public void setTaskRemaining(Long taskRemaining) {
		this.taskRemaining = taskRemaining;
	}

	public Long getResourceType() {
		return resourceType;
	}

	public void setResourceType(Long resourceType) {
		this.resourceType = resourceType;
	}

	public Boolean getIsCompleted() {
		return isCompleted;
	}

	public void setIsCompleted(Boolean isCompleted) {
		this.isCompleted = isCompleted;
	}


	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

}